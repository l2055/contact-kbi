import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
//import org.mockito.Mock;

public class ContactServiceTest {

    //@Mock
  //  private IContactDao contactDao;

    @InjectMocks
    private final ContactService ContactService = new ContactService();

        @Test
        void shouldfailToolong(){
            Assertions.assertThrows(IllegalArgumentException.class,
                    ()-> ContactService.creerContact("123456789ab"));
        }

        @Test
        void shouldfailToolittle(){
            Assertions.assertThrows(IllegalArgumentException.class,
                    ()-> ContactService.creerContact("12"));
        }

    }

